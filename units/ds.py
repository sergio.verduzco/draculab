"""
ds.py
This is a version of the ds_rdc module withouth any rdc units.
"""
from draculab import unit_types, synapse_types, syn_reqs  # names of models and requirements
from units.units import *
import numpy as np


class double_sigma_base(unit):
    """ The parent class for units in the double sigma family. """

    def __init__(self, ID, params, network):
        """ The unit constructor.

        Args:
            ID, params, network: same as in the parent's constructor.
            n_ports is no longer optional.
                'n_ports' : number of inputs ports. n_ports = len(branch_w) + extra_ports

            In addition, params should have the following entries.
                REQUIRED PARAMETERS 
                'slope' : Slope of the global sigmoidal function.
                'thresh' : Threshold of the global sigmoidal function. 
                'branch_params' : A dictionary with the following 3 entries:
                    'branch_w' : The "weights" for all branches. This is a list whose length is the number
                            of branches. Each entry is a positive number.
                            The input port corresponding to a branch is the index of its corresponding 
                            weight in this list, so len(branch_w) = n_ports.
                    'slopes' : Slopes of the branch sigmoidal functions. It can either be a float value
                            (resulting in all values being the same), it can be a list of length n_ports
                            specifying the slope for each branch, or it can be a dictionary specifying a
                            distribution. Currently only the uniform distribution is supported:
                            {..., 'slopes':{'distribution':'uniform', 'low':0.5, 'high':1.5}, ...}
                    'threshs' : Thresholds of the branch sigmoidal functions. It can either be a float
                            value (resulting in all values being the same), it can be a list of length 
                            n_ports specifying the threshold for each branch, or it can be a dictionary 
                            specifying a distribution. Currently only the uniform distribution is supported:
                            {..., 'threshs':{'distribution':'uniform', 'low':-0.1, 'high':0.5}, ...}
                        
                'tau' : Time constant of the update dynamics.
                'phi' : This value is substracted from the branches' contribution. In this manner a branch
                        can have a negative (inhibitory) contribution.

            For units where the number of ports exceeds the number of branches, the
            'extra_ports' variable should be created, so that n_ports = len(branch_w) + extra_ports.
        Raises:
            ValueError, NameError

        """
        # branch_w, slopes, and threshs are inside the branch_params dictionary because if they
        # were lists then network.create_units would interpret them as values to assign to separate units.

        unit.__init__(self, ID, params, network)
        if not hasattr(self,'n_ports'): 
            raise NameError( 'Number of ports should be included in the parameters' )
        self.slope = params['slope']    # slope of the global sigmoidal function
        self.thresh = params['thresh']  # horizontal displacement of the global sigmoidal
        self.tau = params['tau']  # the time constant of the dynamics
        self.phi = params['phi'] # substractive factor for the inner sigmoidals
        self.rtau = 1/self.tau   # because you always use 1/tau instead of tau
        br_pars = params['branch_params']  # to make the following lines shorter
        self.br_w = br_pars['branch_w'] # weight factors for all branches
        if not hasattr(self, 'extra_ports'): # for models with more ports than branches
            self.extra_ports = 0
        if self.n_ports > 1:
            self.multiport = True  # This causes the port_idx list to be created in init_pre_syn_update

        if self.extra_ports > 0:
            n_branches = self.n_ports - self.extra_ports
            txt = ' minus ' + str(self.extra_ports) + ' '
        else:
            n_branches = self.n_ports
            txt = ' '
        self.needs_mp_inp_sum = True # use upd_flat_mp_inp_sum when flat
     
        # testing and initializing the branch parameters
        if n_branches != len(self.br_w):
            raise ValueError('Number of ports'+txt+'should equal the length of the branch_w parameter')
        elif min(self.br_w) <= 0:
            raise ValueError('Elements of branch_w should be positive')
        # Getting rid of this test...
        #elif sum(self.br_w) - 1. > 1e-4:
        #    raise ValueError('Elements of branch_w should add to 1')
        
        if type(br_pars['slopes']) is list: 
            if len(br_pars['slopes']) == n_branches:
                self.slopes = br_pars['slopes']    # slope of the local sigmoidal functions
            else:
                raise ValueError('Number of ports'+txt+'should equal the length of the slopes parameter')
        elif type(br_pars['slopes']) == float or type(br_pars['slopes']) == int:
                self.slopes = [br_pars['slopes']]*n_branches
        elif type(br_pars['slopes']) == dict:
            if br_pars['slopes']['distribution'] == 'uniform':
                self.slopes = np.random.uniform(br_pars['slopes']['low'], br_pars['slopes']['high'], n_branches)
            else:
                raise ValueError('Unknown distribution used to specify branch slopes')
        else:
            raise ValueError('Invalid type for slopes parameter')
        #
        if type(br_pars['threshs']) is list: 
            if len(br_pars['threshs']) == n_branches:
                self.threshs= br_pars['threshs']    # threshold of the local sigmoidal functions
            else:
                raise ValueError('Number of ports'+txt+'should equal the length of the slopes parameter')
        elif type(br_pars['threshs']) == float or type(br_pars['threshs']) == int:
                self.threshs = [br_pars['threshs']]*n_branches
        elif type(br_pars['threshs']) == dict:
            if br_pars['threshs']['distribution'] == 'uniform':
                self.threshs= np.random.uniform(br_pars['threshs']['low'], br_pars['threshs']['high'], n_branches)  
            else:
                raise ValueError('Unknown distribution used to specify branch thresholds')
        else:
            raise ValueError('Invalid type for threshs parameter')

 
    def f(self, thresh, slope, arg):
        """ The sigmoidal function, with parameters given explicitly."""
        #return 1. / (1. + np.exp(-slope*(arg - thresh)))
        return cython_sig(thresh, slope, arg)

    def derivatives(self, y, t):
        """ This function returns the derivative of the activity given its current values. """
        #return ( self.f(self.thresh, self.slope, self.get_mp_input_sum(t)) - y[0] ) * self.rtau
        return ( cython_sig(self.thresh, self.slope, self.get_mp_input_sum(t)) - y[0] ) * self.rtau

 
class double_sigma(double_sigma_base):
    """ 
    Sigmoidal unit with multiple dendritic branches modeled as sigmoidal units.

    This model is inspired by:
    Poirazi et al. 2003 "Pyramidal Neuron as Two-Layer Neural Network" Neuron 37,6:989-999

    Each input belongs to a particular "branch". All inputs from the same branch add 
    linearly, and the sum is fed into a sigmoidal function that produces the output of the branch. 
    The output of all the branches is added linearly to produce the total input to the unit, 
    which is fed into a sigmoidal function to produce the output of the unit.

    The equations can be seen in the "double_sigma_unit" tiddler of the programming notes wiki.

    These type of units implement a type of soft constraint satisfaction. According to how
    the parameters are set, they might activate only when certain input branches receive
    enough stimulation.
    """

    def __init__(self, ID, params, network):
        """ The unit constructor.

        Args:
            ID, params, network: same as in the parent's constructor.
            n_ports is no longer optional.
                'n_ports' : number of inputs ports. 

            In addition, params should have the following entries.
                REQUIRED PARAMETERS 
                'slope' : Slope of the global sigmoidal function.
                'thresh' : Threshold of the global sigmoidal function. 
                'branch_params' : A dictionary with the following 3 entries:
                    'branch_w' : The "weights" for all branches. This is a list whose length is the number
                            of branches. Each entry is a positive number.
                            The input port corresponding to a branch is the index of its corresponding 
                            weight in this list, so len(branch_w) = n_ports.
                    'slopes' : Slopes of the branch sigmoidal functions. It can either be a float value
                            (resulting in all values being the same), it can be a list of length n_ports
                            specifying the slope for each branch, or it can be a dictionary specifying a
                            distribution. Currently only the uniform distribution is supported:
                            {..., 'slopes':{'distribution':'uniform', 'low':0.5, 'high':1.5}, ...}
                    'threshs' : Thresholds of the branch sigmoidal functions. It can either be a float
                            value (resulting in all values being the same), it can be a list of length 
                            n_ports specifying the threshold for each branch, or it can be a dictionary 
                            specifying a distribution. Currently only the uniform distribution is supported:
                            {..., 'threshs':{'distribution':'uniform', 'low':-0.1, 'high':0.5}, ...}
                        
                'tau' : Time constant of the update dynamics.
                'phi' : This value is substracted from the branches' contribution.
        Raises:
            ValueError, NameError

        """
        super(double_sigma, self).__init__(ID, params, network)
    
    def get_mp_input_sum(self, time):
        """ The input function of the double sigma unit. """
        # The two versions below require almost identical times
        #
        # version 1. The big zip
        return sum( [ o*(self.f(th,sl,np.dot(w,i)) - self.phi) for o,th,sl,w,i in 
                      zip(self.br_w, self.threshs, self.slopes, 
                          self.get_mp_weights(time), self.get_mp_inputs(time))  ] )
        # version 2. The accumulator
        #ret = 0.
        #w = self.get_mp_weights(time)
        #inp = self.get_mp_inputs(time)
        #for i in range(self.n_ports):
        #    ret += self.br_w[i] * (self.f(self.threshs[i], self.slopes[i], np.dot(w[i],inp[i]))-self.phi)
        #return ret

    def dt_fun(self, y, s):
        """ Returns the derivative when state is y, at time substep s. """
        inp_sum = sum( [ o * (self.f(th,sl,self.mp_inp_sum[p][s]) - self.phi)
                     for o, th, sl, p in zip(self.br_w, self.threshs, self.slopes,
                     range(self.n_ports)) ] )
        return ( cython_sig(self.thresh, self.slope, inp_sum) - y ) * self.rtau


class double_sigma_normal(double_sigma_base):
    """ A version of double_sigma units where the inputs are normalized.

    Double sigma units are inspired by:
    Poirazi et al. 2003 "Pyramidal Neuron as Two-Layer Neural Network" Neuron 37,6:989-999

    Each input belongs to a particular "branch". All inputs from the same branch
    add linearly, are normalized, and the normalized sum is fed into a sigmoidal function 
    that produces the output of the branch. The output of all the branches is added linearly 
    to produce the total input to the unit, which is fed into a sigmoidal function to produce 
    the output of the unit.

    The equations can be seen in the "double_sigma_unit" tiddler of the programming notes wiki.

    These type of units implement a type of soft constraint satisfaction. According to how
    the parameters are set, they might activate only when certain input branches receive
    enough stimulation.
    """

    def __init__(self, ID, params, network):
        """ The unit constructor.

        Args:
            ID, params, network: same as in the parent's constructor.
            n_ports and tau_slow are  no longer optional.
                'n_ports' : number of inputs ports. 
                'tau_slow' : time constant for the slow low-pass filter.

            In addition, params should have the following entries.
                REQUIRED PARAMETERS 
                'slope' : Slope of the global sigmoidal function.
                'thresh' : Threshold of the global sigmoidal function. 
                'branch_params' : A dictionary with the following 3 entries:
                    'branch_w' : The "weights" for all branches. This is a list whose length is the number
                            of branches. Each entry is a positive number.
                            The input port corresponding to a branch is the index of its corresponding 
                            weight in this list, so len(branch_w) = n_ports.
                    'slopes' : Slopes of the branch sigmoidal functions. It can either be a float value
                            (resulting in all values being the same), it can be a list of length n_ports
                            specifying the slope for each branch, or it can be a dictionary specifying a
                            distribution. Currently only the uniform distribution is supported:
                            {..., 'slopes':{'distribution':'uniform', 'low':0.5, 'high':1.5}, ...}
                    'threshs' : Thresholds of the branch sigmoidal functions. It can either be a float
                            value (resulting in all values being the same), it can be a list of length 
                            n_ports specifying the threshold for each branch, or it can be a dictionary 
                            specifying a distribution. Currently only the uniform distribution is supported:
                            {..., 'threshs':{'distribution':'uniform', 'low':-0.1, 'high':0.5}, ...}
                        
                'tau' : Time constant of the update dynamics.
                'phi' : This value is substracted from the branches' contribution.
        Raises:
            ValueError, NameError

        """
        # branch_w, slopes, and threshs are inside the branch_params dictionary because if they
        # were lists then network.create_units would interpret them as values to assign to separate units.
        super().__init__(ID, params, network)
        self.syn_needs.update([syn_reqs.mp_inputs, syn_reqs.mp_weights,
                               syn_reqs.lpf_slow_mp_inp_sum]) 
     
    def get_mp_input_sum(self, time):
        """ The input function of the normalized double sigma unit. """
        # Removing zero or near-zero values from lpf_slow_mp_inp_sum
        lpf_inp_sum = [np.sign(arry)*(np.maximum(np.abs(arry), 1e-3)) for arry in self.lpf_slow_mp_inp_sum]
        #
        # version 1. The big zip
        return sum( [ o*( self.f(th, sl, (np.dot(w,i)-y) / y) - self.phi ) for o,th,sl,y,w,i in 
                     zip(self.br_w, self.threshs, self.slopes, lpf_inp_sum,
                         self.get_mp_weights(time), self.get_mp_inputs(time))  ] )
        # version 2. The accumulator
        #ret = 0.
        #w = self.get_mp_weights(time)
        #inp = self.get_mp_inputs(time)
        #for i in range(self.n_ports):
        #    ret += self.br_w[i] * (self.f(self.threshs[i], self.slopes[i], 
        #            (np.dot(w[i],inp[i])-lpf_inp_sum[i])/lpf_inp_sum[i]) -  self.phi)
        #return ret
    

class sigma_double_sigma(double_sigma_base):
    """ 
    Sigmoidal with somatic inputs and dendritic branches modeled as sigmoidal units.

    This model is a combination of the sigmoidal and double_sigma models, in recognition that
    a neuron usually receives inputs directly to the soma.

    There are two types of inputs. Somatic inputs arrive at port 0 (the default port), and are
    just like inputs to the sigmoidal units. Dendritic inputs arrive at ports with with ID 
    larger than 0, and are just like inputs to double_sigma units.

    Each dendritic input belongs to a particular "branch". All inputs from the same branch add 
    linearly, and the sum is fed into a sigmoidal function that produces the output of the branch. 
    The output of all the branches is added linearly to produce the total input to the unit, 
    which is fed into a sigmoidal function to produce the output of the unit.

    The equations can be seen in the "double_sigma_unit" tiddler of the programming notes wiki.

    These type of units implement a type of soft constraint satisfaction. According to how
    the parameters are set, they might activate only when certain input branches receive
    enough stimulation.
    """

    def __init__(self, ID, params, network):
        """ The unit constructor.

        Args:
            ID, params, network: same as in the parent's constructor.
            n_ports is no longer optional.
                'n_ports' : number of inputs ports. 

            In addition, params should have the following entries.
                REQUIRED PARAMETERS 
                'slope' : Slope of the global sigmoidal function.
                'thresh' : Threshold of the global sigmoidal function. 
                'branch_params' : A dictionary with the following 3 entries:
                    'branch_w' : The "weights" for all branches. This is a list whose length is the number
                            of branches. Each entry is a positive number.
                            The input port corresponding to a branch is the index of its corresponding 
                            weight in this list, so len(branch_w) = n_ports.
                    'slopes' : Slopes of the branch sigmoidal functions. It can either be a float value
                            (resulting in all values being the same), it can be a list of length n_ports
                            specifying the slope for each branch, or it can be a dictionary specifying a
                            distribution. Currently only the uniform distribution is supported:
                            {..., 'slopes':{'distribution':'uniform', 'low':0.5, 'high':1.5}, ...}
                    'threshs' : Thresholds of the branch sigmoidal functions. It can either be a float
                            value (resulting in all values being the same), it can be a list of length 
                            n_ports specifying the threshold for each branch, or it can be a dictionary 
                            specifying a distribution. Currently only the uniform distribution is supported:
                            {..., 'threshs':{'distribution':'uniform', 'low':-0.1, 'high':0.5}, ...}
                        
                'tau' : Time constant of the update dynamics.
                'phi' : This value is substracted from the branches' contribution.  Since all branch
                        weights are positive, this allows to have negative contributions.
        Raises:
            ValueError, NameError

        """
        self.extra_ports = 1 # Used by the parent class' creator
        super().__init__(ID, params, network)
            
    def get_mp_input_sum(self, time):
        """ The input function of the sigma_double_sigma unit. """
        w = self.get_mp_weights(time)
        inp = self.get_mp_inputs(time)
        # version 1 is slightly faster
        #
        # version 1. The big zip
        return np.dot(w[0], inp[0]) + sum(
                    [ o*(self.f(th,sl,np.dot(w,i)) - self.phi) for o,th,sl,w,i in 
                      zip(self.br_w, self.threshs, self.slopes, w[1:], inp[1:]) ] )
        # version 2. The accumulator
        #ret = np.dot(w[0], inp[0])
        #for i in range(1,self.n_ports):
        #    ret += self.br_w[i-1] * (self.f(self.threshs[i-1], self.slopes[i-1], 
        #                             np.dot(w[i],inp[i])) - self.phi)
        #return ret
    
    def dt_fun(self, y, s):
        """ Returns the derivative when state is y, at time substep s. """
        inp_sum = self.mp_inp_sum[0][s] # input sum at port 0
        inp_sum += sum( [ o * (self.f(th,sl,self.mp_inp_sum[p][s]) - self.phi)
                     for o, th, sl, p in zip(self.br_w, self.threshs, self.slopes,
                     range(1,self.n_ports)) ] )
        return ( cython_sig(self.thresh, self.slope, inp_sum) - y ) * self.rtau



class sigma_double_sigma_normal(double_sigma_base):
    """ sigma_double_sigma unit with  normalized branch inputs.

    Double sigma units are inspired by:
    Poirazi et al. 2003 "Pyramidal Neuron as Two-Layer Neural Network" Neuron 37,6:989-999

    There are two types of inputs. Somatic inputs arrive at port 0 (the default port), and are
    just like inputs to the sigmoidal units. Dendritic inputs arrive at ports with with ID 
    larger than 0, and are just like inputs to double_sigma units.

    Each dendritic input belongs to a particular "branch". All inputs from the same branch
    add linearly, are normalized, and the normalized sum is fed into a sigmoidal function 
    that produces the output of the branch. The output of all the branches is added linearly 
    to the somatic input to produce the total input to the unit, which is fed into a sigmoidal 
    function to produce the output of the unit.

    ~~~~~~~~~~ In this version the somatic inputs are not normalized ~~~~~~~~~~

    The equations can be seen in the "double_sigma_unit" tiddler of the programming notes wiki.
    """

    def __init__(self, ID, params, network):
        """ The unit constructor.

        Args:
            ID, params, network: same as in the parent's constructor.
            n_ports, tau_fast and tau_slow are  no longer optional.
                'n_ports' : number of inputs ports. 
                'tau_slow' : time constant for the slow low-pass filter.

            In addition, params should have the following entries.
                REQUIRED PARAMETERS 
                'slope' : Slope of the global sigmoidal function.
                'thresh' : Threshold of the global sigmoidal function. 
                'branch_params' : A dictionary with the following 3 entries:
                    branch_w: The "weights" for all branches. This is a list whose length is the number
                            of branches. Each entry is a positive number.
                            The input port corresponding to a branch is the index of its corresponding 
                            weight in this list plus one. For example, if branch_w=[.2, .8], then
                            input port 0 is at the soma, input port 1 has weight .2, and port 2 weight .8 .
                    'slopes' : Slopes of the branch sigmoidal functions. It can either be a float value
                            (resulting in all values being the same), it can be a list of length n_ports
                            specifying the slope for each branch, or it can be a dictionary specifying a
                            distribution. Currently only the uniform distribution is supported:
                            {..., 'slopes':{'distribution':'uniform', 'low':0.5, 'high':1.5}, ...}
                    'threshs' : Thresholds of the branch sigmoidal functions. It can either be a float
                            value (resulting in all values being the same), it can be a list of length 
                            n_ports specifying the threshold for each branch, or it can be a dictionary 
                            specifying a distribution. Currently only the uniform distribution is supported:
                            {..., 'threshs':{'distribution':'uniform', 'low':-0.1, 'high':0.5}, ...}
                        
                'tau' : Time constant of the update dynamics.
                'phi' : This value is substracted from the branches' contribution.  Since all branch
                        weights are positive, this allows to have negative contributions.
                
        """
        self.extra_ports = 1 # The soma port        
        double_sigma_base.__init__(self, ID, params, network)
        self.syn_needs.update([syn_reqs.mp_inputs, syn_reqs.mp_weights,
                               syn_reqs.lpf_slow_mp_inp_sum]) 
     
    
    def get_mp_input_sum(self, time):
        """ The input function of the normalized double sigma unit. """
        # Removing zero or near-zero values from lpf_slow_mp_inp_sum
        lpf_inp_sum = [np.sign(arry)*(np.maximum(np.abs(arry), 1e-3)) for arry in self.lpf_slow_mp_inp_sum]
        w = self.get_mp_weights(time)
        inp = self.get_mp_inputs(time)
         
        return np.dot(w[0], inp[0]) + sum( [ o*( self.f(th, sl, (np.dot(w,i)-y) / y) - self.phi ) 
               for o,th,sl,y,w,i in zip(self.br_w, self.threshs, self.slopes, lpf_inp_sum[1:], w[1:], inp[1:]) ] )
 


